/**
 * Payment Manager Model
 *
 * @class Payment Manager
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PaymentmanagerhoneymeOrder = DS.Model.extend({
	'orderDate': DS.attr('string'),
});

EmberApp.PaymentmanagerhoneymeOrderAdapter = EmberApp.PaymentmanagerhoneymeAPIAdapter.extend({});

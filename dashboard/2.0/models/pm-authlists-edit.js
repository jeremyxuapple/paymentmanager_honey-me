/**
 * Payment Manager Model
 *
 * @class Payment Manager
 * @extends DS.Model
 * @namespace EmberApp
 *
*/
EmberApp.PmAuthlistsEdit = DS.Model.extend({
	'orderLists' : DS.attr(),
});

EmberApp.PmAuthlistsEditAdapter = EmberApp.PaymentmanagerhoneymeAPIAdapter.extend({});

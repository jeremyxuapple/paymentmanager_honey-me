
/**
* Payment Manager App Router
*/
EmberApp.Router.map(function() {
	this.resource('paymentmanagerhoneyme', { path: '/apps/paymentmanagerhoneyme' }, function() {
		this.route('search', { path: '/search' });
		this.route('authlists', { path: '/authlists' }, function() {
			this.route('edit', { path: '/:id' });
		});
		this.route('instockcapture', { path: '/instockcapture' });
		
		this.route('appmanagement', { path: '/appmanagement' }, function() {
			this.route('emailsettings', { path: '/emailsettings'});
			this.route('emailtemplates', { path: '/emailtemplates'}, function() {
				this.route('edit', { path: '/:id'});
			});
		});
	});
});


/**
 * Adapter for Payment Manager Models
 *
 * @class PMAPIAdapter
 * @extends DS.RESTAdapter
 * @namspace EmberApp
 */
EmberApp.PaymentmanagerhoneymeAPIAdapter = DS.RESTAdapter.extend({
	namespace: 'customer/apps/paymentmanagerhoneyme'
});

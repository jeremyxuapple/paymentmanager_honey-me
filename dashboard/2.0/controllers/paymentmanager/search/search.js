EmberApp.PaymentmanagerhoneymeSearchController = Ember.Controller.extend({

	'Loading': true,
	'searchInProgress': false,
	'noResults': false,
	'searchSection': true,
	'selectOptions': { 'width': '30%', 'placeholder': 'Choose options ...' },

	'getOrderDates': function() {
		var url = '/customer/apps/paymentmanagerhoneyme/searchOrderDate';
		var controller = this;

		Ember.$.ajax({
			'url': url,
			'type': 'GET',
			'dataType': 'json'
		}).done(function (res) {
			controller.set('orderDates', res);
			controller.set('Loading', false);
		});

	}.on('init'),

	'actions': {
		'searchOrder': function() {
			var controller = this;
			var order = controller.get('model');
				controller.set('searchInProgress', true);
				controller.set('noResults', false);
			var orderDate = order.get('orderDate').label;
			var url = '/customer/apps/paymentmanagerhoneyme/searchOrder';

			Ember.$.ajax({
				'url':url,
				'type': 'GET',
				'data': {
					'orderDate': orderDate,
				},
				'dataType': 'json'
			}).done(function(res) {

				if (res.length == 0) {
					controller.set('noResults', true);
				}
					controller.set('orders', res);
					controller.set('searchSection', false);
					controller.set('searchInProgress', false);
					controller.set('orderDate', order.get('orderDate'));
			}).fail(function(err) {
				alert(err.responseJSON.msg);
			})
		}, // End of the searchOrder function
    //
		// 'setAuthroizeOrders': function(ordersForAuthorize) {
		// 	this.set('ordersForAuthorize', ordersForAuthorize);
		// 	this.set('orders', false);
		// },

		'setAuthorizeResponseList': function(authorizeResponseList) {
			this.set('authorizeResponseList', authorizeResponseList);
			this.set('orders', false);
		},

		'newSearch': function() {
			window.location.reload();
		},

	}
});

EmberApp.PaymentmanagerhoneymeAppmanagementEmailsettingsController = Ember.Controller.extend({
	'app': Ember.inject.controller('paymentmanagerhoneyme'),
	'actions': {
		'saveEmail': function() {
  			var settings = this.get('settings');
        var url = '/customer/apps/paymentmanagerhoneyme/saveEmailsettings';
        var controller = this;

        Ember.$.ajax({
            'url': url,
            'type': 'POST',
            'data': {
                      'settings': settings
                    },
            'dataType': 'json'
        }).success(function(resp) {
                    try {
                       if(JSON.parse(resp).success == false){
                          controller.notifications.show('No information was found.', 'Success', 'success');
                       }else{
                          controller.notifications.show('Failed to download.', 'Error', 'error');
                       }
                    } catch (e) {
                        controller.notifications.show('Settings Update Successfully', 'Success', 'success');
                    }
                 }).fail(function() {
                    controller.notifications.show('Failed to download.', 'Error', 'error');
                 });
		  },

    'testSMTP': function() {
      var url = '/customer/apps/paymentmanagerhoneyme/SMTPTesting';
      var controller = this;
      Ember.$.ajax({
            'url': url,
            'type': 'GET',
            'dataType': 'json'
        }).success(function(resp) {
            try {
               if(JSON.parse(resp).success == false){
                  controller.notifications.show('No information was found.', 'Success', 'success');
               }else{
                  controller.notifications.show('Failed to send out testing', 'Error', 'error');
               }
            } catch (e) {
                controller.notifications.show('Please check you email', 'Success', 'success');
            }
         }).fail(function() {
            controller.notifications.show('Failed to send out testing.', 'Error', 'error');
         });
    }
	}
});

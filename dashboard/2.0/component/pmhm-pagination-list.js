EmberApp.PmhmPaginationListComponent = Ember.Component.extend({
	'tagName': 'div',
	'classNames': ['pagination-wrapper'],
	'selectOptions': { 'width': '20%', 'placeholder': 'Update Items Per Page..' },
    'pageItemsOptions':  [
      {id : 0, label : "25"},
      {id : 1, label : "50"},
      {id : 2, label : "100"},
      ],
	'total': 1,
	'selected': 1,
	'pagesToDisplay': 5,
  'unprocessed': false,
  'itemsPerPage': 0,
  'itemsPerPage1': 0,
	'clickAction': 'changePage',
  'clickAction1': 'changePage1',

	'showPagination': function() {
		var total = this.get('total');
		return (total && parseInt(total) > 1);
	}.property('total'),

	'pages': function() {
		var total = parseInt(this.get('total'));
		var selected = parseInt(this.get('selected'));
		var displayLimit = parseInt(this.get('pagesToDisplay'));

		// calculate number of pages
		var pageItems = [];

		// calculate which page to show
		if (total <= displayLimit) {
            pageItems = this.addRange(1, total, selected, pageItems);
        } else {
            var diff = Math.floor((displayLimit - 1) / 2);

            var start = selected - diff;
            if (start < 1) start = 1;

            var end = start + displayLimit - 1;
            if (end > total) end = total;

            var pageObj;

            if (start > 1) {
                // add item to skip to first page
                pageObj = {
                    'page': 1,
                    'label': '«',
                    'selected': false,
                    'isFirst': true,
                    'isLast': false
                };

                pageItems.addObject(pageObj);
            }

            // re-adjust starting page number
            var numOfPages = end - start + 1;

            if (numOfPages < displayLimit) {
                var addToStart = ((selected - start) < (end - selected));

                while (numOfPages < displayLimit) {
                    if (addToStart) {
                        if (start > 1) {
                            start--;
                            numOfPages++;
                        }
                    } else {
                        if ((end + 1) < total) {
                            end++;
                            numOfPages++;
                        }
                    }

                    addToStart = (!addToStart);
                }
            }

            // add range to pagination
            pageItems = this.addRange(start, end, selected, pageItems);

            if (end < total) {
                // add item to skip to last page
                pageObj = {
                    'page': total,
                    'label': '»',
                    'selected': false,
                    'isFirst': false,
                    'isLast': true
                };

                pageItems.addObject(pageObj);
            }
        }

		return pageItems;
	}.property('total', 'selected'),

    'addRange': function(start, end, selected, pageItems) {
        for (var pageNum = start; pageNum <= end; pageNum++) {
            var pageObj = {
                'page': pageNum,
                'label': pageNum,
                'selected': (pageNum === selected),
                'isFirst': false,
                'isLast': false
            };

            pageItems.addObject(pageObj);
        }

        return pageItems;
    },

    'didInsertElement': function () {
        var component = this;
        var options = this.get('selectOptions');

        var select = this.$('select');
        // for the unprocessed authroized items only
        $('.unprocessed').change(function(){
            var itemsPerPage = $(this).children(':selected')[0].text;
            component.set('itemsPerPage1', itemsPerPage);
            component.sendAction('clickAction1', 1, itemsPerPage);
        });

        select.select2(options).on('change', function (e) {
            var itemsPerPage = select.select2('data')[0].text;
            component.set('itemsPerPage', itemsPerPage);

            // After the itemsPerPage has been updated, we will send out the pageNum = 1 as the second paramenter
            // and the itemsPerPage the third parameter to the uppper level component
            component.sendAction('clickAction', 1, itemsPerPage);
        });
    },

	'actions': {
		'selectItem': function(pageNum) {
            var itemsPerPage = this.get('itemsPerPage');
            // console.log('itemsPerPage', '++', itemsPerPage);
			this.sendAction('clickAction', pageNum, itemsPerPage);
			this.set('selected', pageNum);
		},

        // for the unprocessed authroized items only
        'selectItem1': function(pageNum) {
            var itemsPerPage = this.get('itemsPerPage1');

            this.sendAction('clickAction1', pageNum, itemsPerPage);
            this.set('selected', pageNum);
        }
	}
});

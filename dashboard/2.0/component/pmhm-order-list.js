/**
 * Payment Manager Orders List
 */
EmberApp.PmhmOrderListComponent = Ember.Component.extend({
    'tagName': 'ul',
    'classNames': [ 'pm-product-list', 'table-list', 'products' ],
	  'selectedItems': [],
	  'currentPage': 1,
    'itemsPerPage': 25,
    'itemsToShow': [],

    'didInsertElement': function() {
      this.changePage(1, this.get('itemsPerPage'));
      // this.sendAction('changePage', 1);
      var productName = this.get('orders')[0]['name'];
      this.set('productName', productName);
    },

    /**
     * returns the number of pages to display
     *
     * @method maxPages
     * @return {Number}
     */
    'maxPages': function() {
        var orders = this.get('orders'),
            itemsPerPage = this.get('itemsPerPage');
        if (orders) return Math.ceil(orders.length / itemsPerPage);
        else return 1;
    }.property('orders', 'itemsPerPage'),

    'changePage': function(pageNum, itemsPerPage) {
      this.set('itemsPerPage', itemsPerPage);
      var controller = this,
          orders = this.get('orders'),
          pageNum = parseInt(pageNum),
          itemsPerPage = parseInt(itemsPerPage),

          start = (pageNum - 1) * itemsPerPage,
          end = start + itemsPerPage - 1;
          items = orders.slice(start, end + 1);

       controller.set('itemsToShow', items);
    },

    'actions': {
       /**
       * show the results for a specific page
       *
       * @method changePage
       * @param {Number} pageNum - page number
       */
       'changePage': function(pageNum, itemsPerPage) {
          this.changePage(pageNum, itemsPerPage);
      	},

       'newSearch': function() {
    			window.location.reload();
    		},

        'toggleAll': function() {

    			var selectAll = this.get('selectAll'),
    				selectedItems = this.get('selectedItems');

    			if (selectAll) {
    				selectedItems.clear();
    			} else {
    				var orders = this.get('orders');

          orders.forEach( function(order) {

  					if (selectedItems.length == 0) {
    						selectedItems.pushObject({
    							 	'id': order.id,
    							 	'authAmount': order.authAmount
    							 });
    					} else {
    						for (var i = 0; i < selectedItems.length; i++) {
    							if (selectedItems[i].id == order.id) {
    								break;
    							} else if (i == selectedItems.length - 1) {
    								selectedItems.pushObject({
    								 	'id': order.id,
    								 	'authAmount': order.authAmount
    								 });
    							}
    						}
    					}

            });

    			}

    			this.set('selectAll', (!selectAll)).set('selectedItems', selectedItems);

    		},

    'selectItem': function(orderId, authAmount, selected) {
      // console.log(selected);

			var controller = this;
			var selectedItems = controller.get('selectedItems');

			if (selected) {
				// When the selectedItems array is empty, push the order directly
				if (selectedItems.length == 0) {
						selectedItems.pushObject({
							 	'id': orderId,
							 	'authAmount':authAmount
							 });
					// otherwise check if the order is already in the selectedItems array
					} else {
						for (var i = 0; i < selectedItems.length; i++) {
							if (selectedItems[i].id == orderId) {
								break;
							} else if (i == selectedItems.length - 1) {
								selectedItems.pushObject({
								 	'id': orderId,
								 	'authAmount': authAmount
								 });
							}
						}
					}

				// when all the items has been selected, we will update the selecteAll property
				if (selectedItems.length == controller.get('orders').length) {
					controller.set('selectAll', (!controller.get('selectAll')));
				}

			} else {
        // console.log('need to remove');
				selectedItems.forEach(function(selectedItem) {
					if (selectedItem.id == orderId) {
						selectedItems.removeObject(selectedItem);
						// if the toggle all has been checked, we need to unchecked it
						if (controller.get('selectAll')) {
							controller.set('selectAll', (!controller.get('selectAll')));
						}
					}
				});
			}
      // console.log(selectedItems);
			controller.set('selectedItems', selectedItems);
		},

		'authorizeOrders': function() {
			var url = '/customer/apps/paymentmanagerhoneyme/authorizeOrders';
			var controller = this;
      controller.set('authorizing', true);
			Ember.$.ajax({
				'url': url,
				'type': 'POST',
				'data': {
							'orders': controller.get('selectedItems'),
						},
				'dataType': 'json'
			}).done(function(res) {
        controller.set('shippingFeePopup', false);
				controller.sendAction('authorizeResponseList', res);
        controller.set('authorizing', false);
			});
		},

		'exportCSV': function() {

			var url = '/customer/apps/paymentmanagerhoneyme/searchOrder';
			var controller = this;
			Ember.$.ajax({
				'url': url,
				'type': 'GET',
				'data': {
							'exportCSV': true,
              'orderDate': controller.get('orderDate').label
						},
				'dataType': 'text'
			}).success(function(resp) {
            try {
               if(JSON.parse(resp).success == false){
                  controller.notifications.show('No information was found.', 'Success', 'success');
               }else{
                  controller.notifications.show('Failed to download.', 'Error', 'error');
               }
            } catch (e) {
               	controller.notifications.show('Download will start soon.', 'Success', 'success');
               	var url = "data:text/csv,"+encodeURIComponent(resp);
               	var a = $("<a />", {
                  	href: url,
                  	download: "searchResult.csv"
               	})
               	.appendTo("body")
               	.get(0)
               	.click();
            }
         }).fail(function() {
            controller.notifications.show('Failed to download.', 'Error', 'error');
         });
			},

      'shippingFeePopup': function() {
        this.set('addShippingFees', true);
      },

      'addShippingPercentage': function() {
        var shippingPercentage = $('#shippingPercentage')[0].value;

        var orders = this.get('orders'),
          selectedItems = this.get('selectedItems');

        orders.map( function (order) {
          order['shippingFees'] = 0;
          order['authAmount'] = 0;
        });

        selectedItems.map(function (item) {
          orders.map( function(order) {
            if (item.id === order.id) {

              order['shippingFees'] = item['shippingFees'] = (parseFloat(order.total_inc_tax.replace(/,/g, '')) * shippingPercentage / 100).toFixed(2);
              order['authAmount'] = item['authAmount'] = (parseFloat(order.total_inc_tax.replace(/,/g, '')) + parseFloat(order.shippingFees)).toFixed(2);
            }
          });
        });

        this.set('shippingColumn', true);
        this.set('addShippingFees', false);
      },

      'closeShippingFees': function () {
        this.set('addShippingFees', false);
      }

    } // End of the actions

});

EmberApp.PmhmInstockListComponent = Ember.Component.extend({
  'classNames':['instock-list'],
  'selectedItems': [],
  'currentPage': 1,
  'itemsPerPage': 25,
  'itemsToShow': [],
  'orderList': true,
  'authorizing': false,

  'didInsertElement': function() {
    this.changePage(1, this.get('itemsPerPage'));
  },

  /**
   * returns the number of pages to display
   *
   * @method maxPages
   * @return {Number}
   */
  'maxPages': function() {
      var orders = this.get('rows'),
          itemsPerPage = this.get('itemsPerPage');
      if (orders) return Math.ceil(orders.length / itemsPerPage);
      else return 1;
  }.property('orders', 'itemsPerPage'),

  'changePage': function(pageNum, itemsPerPage) {
    this.set('itemsPerPage', itemsPerPage);
    var controller = this,
        orders = this.get('rows'),
        pageNum = parseInt(pageNum),
        itemsPerPage = parseInt(itemsPerPage),

        start = (pageNum - 1) * itemsPerPage,
        end = start + itemsPerPage - 1;
        items = orders.slice(start, end + 1);

        controller.set('itemsToShow', items);
    },




  'actions': {

    'showOrdersList': function() {
      this.set('orderList', true);
    },

     'changePage': function(pageNum, itemsPerPage) {
        this.changePage(pageNum, itemsPerPage);
      },

      'toggleAll': function() {

        var selectAll = this.get('selectAll'),
          selectedItems = this.get('selectedItems');

        if (selectAll) {
          selectedItems.clear();
        } else {
          var orders = this.get('rows');

        orders.forEach( function(order) {

          if (selectedItems.length == 0) {
              selectedItems.pushObject({
                  'order_id': order.order_id,
                  'authAmount': order.capture_amount
                 });
            } else {
              for (var i = 0; i < selectedItems.length; i++) {
                if (selectedItems[i].order_id == order.order_id) {
                  break;
                } else if (i == selectedItems.length - 1) {
                  selectedItems.pushObject({
                    'order_id': order.order_id,
                    'authAmount': order.capture_amount
                   });
                }
              }
            }

          });

        }

        this.set('selectAll', (!selectAll)).set('selectedItems', selectedItems);

      },

  'selectItem': function(orderId, authAmount, selected) {

    var controller = this;
    var selectedItems = controller.get('selectedItems');

    if (selected) {
      // When the selectedItems array is empty, push the order directly
      if (selectedItems.length == 0) {
          selectedItems.pushObject({
              'order_id': orderId,
              'authAmount':authAmount
             });
        // otherwise check if the order is already in the selectedItems array
        } else {
          for (var i = 0; i < selectedItems.length; i++) {
            if (selectedItems[i].id == orderId) {
              break;
            } else if (i == selectedItems.length - 1) {
              selectedItems.pushObject({
                'order_id': orderId,
                'authAmount': authAmount
               });
            }
          }
        }

      // when all the items has been selected, we will update the selecteAll property
      if (selectedItems.length == controller.get('rows').length) {
        controller.set('selectAll', (!controller.get('selectAll')));
      }

    } else {
      // console.log('need to remove');
      selectedItems.forEach(function(selectedItem) {
        if (selectedItem.order_id == orderId) {
          selectedItems.removeObject(selectedItem);
          // if the toggle all has been checked, we need to unchecked it
          if (controller.get('selectAll')) {
            controller.set('selectAll', (!controller.get('selectAll')));
          }
        }
      });
    }
    // console.log(selectedItems);
    controller.set('selectedItems', selectedItems);
  },

  'authorizeOrders': function() {
      var url = '/customer/apps/paymentmanagerhoneyme/authorizeOrders';
      var controller = this;
      controller.set('authorizing', true);
      Ember.$.ajax({
        'url': url,
        'type': 'POST',
        'data': {
              'orders': controller.get('selectedItems'),
              'paymentTransactionType': 'authCaptureTransaction'
            },
        'dataType': 'json'
      }).done(function(res) {
        controller.set('authorizing', false);
        controller.set('captureResults', res);
        controller.set('orderList', false);
      });
    },

  } // End of the actions

});

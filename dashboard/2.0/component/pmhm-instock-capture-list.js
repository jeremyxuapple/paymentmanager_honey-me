EmberApp.PmhmInstockCaptureListComponent = Ember.Component.extend({
  'heads':['Status', 'Transaction ID', 'Failed Message', 'Order', 'Customer Name', 'Customer Email', 'Charge Amount'],
  'currentPage': 1,
  'itemsPerPage': 25,
  'itemsToShow': [],

  'didInsertElement': function() {
    this.changePage(1, this.get('itemsPerPage'));
  },

  'maxPages': function() {
      var orders = this.get('captureResults'),
          itemsPerPage = this.get('itemsPerPage');
      if (orders) return Math.ceil(orders.length / itemsPerPage);
      else return 1;
  }.property('orders', 'itemsPerPage'),

  'changePage': function(pageNum, itemsPerPage) {
    this.set('itemsPerPage', itemsPerPage);
    var controller = this,
        orders = this.get('captureResults'),
        pageNum = parseInt(pageNum),
        itemsPerPage = parseInt(itemsPerPage),

        start = (pageNum - 1) * itemsPerPage,
        end = start + itemsPerPage - 1;
        items = orders.slice(start, end + 1);
        controller.set('itemsToShow', items);
  },

  'actions': {
    'changePage': function(pageNum, itemsPerPage) {
       this.changePage(pageNum, itemsPerPage);
     },

     'printSummary': function () {
       window.print();
     },

     'showOrders': function() {
       this.sendAction('showOrders');
     },

  }
});

EmberApp.PmhmPaginationItemComponent = Ember.Component.extend({
	'tagName': 'li',
	'classNames': ['pagination-item'],
	'classNameBindings': ['item.selected:selected', 'item.isFirst:first', 'item.isLast:last'],
	'unprocessed': false,
    'clickAction': 'selectPage',

    'actions': {
		'selectItem': function(pageNum) {
			this.sendAction('clickAction', pageNum);
		},
		'selectItem1': function(pageNum) {
			this.sendAction('clickAction1', pageNum);
		}
	}
});
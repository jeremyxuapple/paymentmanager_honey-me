/**
 * Payment Manager Saved Authorize List Item Edit Component for controlling
 *	the logic of item adjustment or cancellation
 */

EmberApp.PmhmSavedAuthorizeListEditItemComponent = Ember.Component.extend({
	'tagName': '',
  'canceled': false,

  'didInsertElement': function() {
    if (this.get('order').payment_status == 'Void') {
      this.set('canceled', true);
    }
  },

  'actions': {
  	'captureItem': function() {
			console.log('hhh');
  		var total = this.get('order').auth_total,
            id = this.get('order').id;
  		this.sendAction('captureItemPopup', id, total, true);
  	},

  	'cancelItem': function() {
  		var order = this.get('order');
      var id = order.id;
      this.sendAction('cancelPopupFlag', id);
  	}
  }
});


EmberApp.PmhmInstockListItemComponent = Ember.Component.extend({
  'classNames': ['instock-row'],
  'adjustpopup': false,

  'selected':[],

	'isSelected': function() {
		var selected = this.get('selected'),
			orderId = this.get('order.order_id');
			found = false;
      selected.forEach(function (selectedItem) {
      	if (selectedItem.order_id == orderId) {
      		found = true;
      	}
      });
			return found;
	}.property('selected.[]', 'order.order_id'),

    'actions': {
			'toggleItem': function() {

				var selected = this.get('isSelected'),
					orderId = this.get('order.order_id'),
					authAmount = this.get('order.capture_amount');

				this.sendAction('selectAction', orderId, authAmount, (!selected));
			},
      'showAdjustModal': function() {
        this.set('adjustpopup', true);
      },
      'closeAdjustModal': function() {
        this.set('adjustpopup', false);
      },
      'adjustCaptureAmount': function() {
        this.set('adjustpopup', false);
      }
    }
});

 /**
 * Payment Manager Orders List Item
 */
EmberApp.PmhmOrderListItemComponent = Ember.Component.extend({
	'tagName': '',
  'index': 0,
  'total': 1,
	'selected': [],

	'isSelected': function() {
		var selected = this.get('selected'),
			orderId = this.get('order.id');
			found = false;
      selected.forEach(function (selectedItem) {
      	if (selectedItem.id == orderId) {
      		found = true;
      	}
      });
			return found;
	}.property('selected.[]', 'order.id'),

    'actions': {
			'toggleItem': function() {
				var selected = this.get('isSelected'),
					orderId = this.get('order.id'),
					authAmount = this.get('order.authAmount');

				this.sendAction('selectAction', orderId, authAmount, (!selected));
			}
    }
});

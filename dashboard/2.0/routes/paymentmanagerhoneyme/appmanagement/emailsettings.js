EmberApp.PaymentmanagerhoneymeAppmanagementEmailsettingsRoute = Ember.Route.extend({
	'model': function() {
        return Ember.$.get('/customer/apps/paymentmanagerhoneyme/getEmailSettings');
    },

    setupController: function(controller, model) {
    	controller.set('settings', model);
        this._super(controller, model);
    },

});


EmberApp.PaymentmanagerhoneymeRoute = Ember.Route.extend({
    /**
     * retrieve application information from the data store
     *
     * @function model
     * @returns {RSVP.Promise}
     */

    model: function() {

    	return Ember.RSVP.all([
    		this.store.find('app', 'paymentmanagerhoneyme')
    	]);
    },

    afterModel:function(model) {
    	var applicationController = this.controllerFor('application');

        var menuItems = appMenuItems.get('paymentmanagerhoneyme');
        applicationController.set('appsMenu', menuItems);
    },

    setupController: function(controller, model){
    	this._super(model, controller);
    	var app = model.objectAt(0);

    	var menuItems = appMenuItems.get('paymentmanagerhoneyme');
            
    	controller
    		.set('app', app)
    		.set('appsMenu', menuItems);
    },

    actions: {
    	'clearSearch': function() {
    		 this.controller.send('clearSearch');
    	}
    }


});

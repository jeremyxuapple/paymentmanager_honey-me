<?php

namespace MiniBC\addons\paymentmanagerhoneyme\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\Log;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


use MiniBC\addons\recurring\services\PaymentService;
use MiniBC\addons\recurring\services\PaymentProfileService;

class AuthorizeController {
		private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
        $this->db = ConnectionManager::getInstance('mysql');
        $this->customer = Auth::getInstance()->getCustomer();
        $this->store = $this->customer->stores[0];
    }

    /**
    * Authorize Orders
    */

    public function authorizeOrders()
    {
        $customer_store_id = $this->customer->id;
        $authroizeList = array();
        $orders = $_POST['orders'];

        foreach ($orders as $order) {

            $orderId = isset($order['id']) ? $order['id'] : $order['order_id'];
            $amount = $order['authAmount'];

            $paymentProfileQuery = "SELECT rpp.id, rpp.gateway
                                    FROM rc_payment_profiles rpp
                                    JOIN bigbackup_bc_orders o
                                    JOIN rc_customer_profiles rcp
                                        ON rcp.store_customer_id = o.bc_customer_id
                                        AND rcp.profile_id = rpp.customer_profile_id
                                    WHERE o.bc_id = $orderId
                                    AND o.customer_id = $customer_store_id
                                    AND rpp.customer_id = $customer_store_id
                                    AND rcp.customer_id =$customer_store_id
                                    ";

            $paymentData = $this->db->queryFirst($paymentProfileQuery);
            $baseInfo = $this->getCustomerInformation($orderId);
						$baseInfo['authAmount'] = $amount;

          try {

	    			$gateway = PaymentService::getInstance()->getGateway($paymentData['gateway'], $this->store);

	    			$profile = PaymentProfileService::getInstance()->getFromId($paymentData['id'], $this->store);

	    			// set gateway to authorize only mode
						$gateway->paymentTransactionType = 'authOnlyTransaction';
						if (isset($_POST['paymentTransactionType'])) $gateway->paymentTransactionType = $_POST['paymentTransactionType'];

	    			// set metadata for the gateway
	    			$metadata = array('orderId' => $orderId);

	    			// create transaction
	    			$response = $gateway->createTokenizedPayment($amount, $profile, $metadata);

	                $responseLog = get_object_vars($response);
	                Log::addInfo('authorize response:', $responseLog);

	                $baseInfo['transactionId'] = $response->getTransactionId();
	                $baseInfo['gateway'] = $paymentData['gateway'];

	    			if ($response->getStatus() == 'payment_success') {
	                $baseInfo['payment_status'] = 'Success';
	    			} else {
	    				$baseInfo['payment_status'] = 'Failed';
	    			}
              $baseInfo['message'] = $response->getMessage();
    		} catch (\Exception $e) {
    			// handle exception here
    		}

        array_push($authroizeList, $baseInfo);

        } // End of the for loop

        return JsonResponse::create($authroizeList);
    }

    /**
    * Save the authorize response list into the database for later display and udpate
    */

    public function saveAuthList()
    {
        $customer_id = $this->customer->id;
        $listName = $_POST['listName'];
        $orders = $_POST['orders'];

        $authList = array(
            'name' => $listName,
            'customer_id' => $customer_id,
            'status' => 'New',
            'orders_containing' => sizeof($orders),
            'date_created' => time(),
            'date_updated' => time()
        );

        $authListId = $this->db->insert('pm_auth_list', $authList);

        foreach ($orders as $order) {
						$orderId = $order['bc_id'];
						$authAmt = $order['authAmount'];
						$orderValue = $this->db->queryFirst("SELECT total_inc_tax FROM bigbackup_bc_orders WHERE customer_id=$customer_id AND bc_id=$orderId")['total_inc_tax'];
						$shippingFees = $authAmt - $orderValue;
	           $authItem = array(
	              'pm_auth_list_id' => $authListId,
	              'order_id' => $orderId,
	              'customer_id' => $customer_id,
	              'bc_customer_id' => $order['bc_customer_id'],
	              'payment_status' => $order['payment_status'] == 'Failed' ? 'Failed' : 'Authorized',
	              'message' => $order['message'],
	              'gateway' => $order['gateway'],
	              'last4' => $order['last4'],
	              'expiry' => $order['expiry'],
	              'transactionId' => $order['transactionId'],
	              'customer_name' => $order['customer_name'],
	              'billing_email' => $order['billing_email'],
								'shipping_fees' => $shippingFees,
	              'product_name' => '',
	              'product_qty' => '',
	              'product_sku' => '',
	              'auth_total' => $authAmt,
	              'date_created' => time(),
	              'date_updated' => time()
            );

            $this->db->insert('pm_auth_item', $authItem);

         }
        $response = array('status' => 'success');
        return JsonResponse::create($response);
    }

    /*
    * Pulling the authorize list which have been saved into the database
    */
    public function getAuthLists()
    {
        $customer_id = $this->customer->id;
        $authLists = $this->db->query("
            SELECT id, name, status, orders_containing, from_unixtime(date_created) AS date_created
            FROM pm_auth_list
            WHERE customer_id = $customer_id
            ORDER BY date_created DESC
            ");

        return JsonResponse::create($authLists);
    }

    /**
    * Pulling the detailed orders of a specific authorize list which have been saved into the database
    */
    public function getAuthEditLists($id)
    {
        $response['pm-authlists-edit'] = array();
        $customerStoreId = $this->customer->id;
        $list = $this->db->query("
            SELECT * FROM pm_auth_item
            WHERE pm_auth_list_id = $id
            AND customer_id = $customerStoreId
            ");
        $response['pm-authlists-edit']['id'] = $id;
        $response['pm-authlists-edit']['orderLists'] = $list;

				if (isset($_GET['export'])) {
					$this->adjustShippingFees($list);
				}

				if (isset($_GET['authResult'])) {
					$this->exportAuthReport($list);
				}

        return JsonResponse::create($response);
    }

    /**
    * Void the authorized transactions
    * @param - $id: the id of the order needed to be voided.
    */
    public function cancelAuthItem($id)
    {
        if ($id == null) {
            $id = $_POST['id'];
        }

        $orderInfo = $this->db->queryFirst("
            SELECT gateway, transactionId FROM pm_auth_item WHERE id = $id
            ");

        try {
              $gateway = PaymentService::getInstance()->getGateway($orderInfo['gateway'], $this->store);

              $response = $gateway->voidAuthorizedPayment($orderInfo['transactionId']);

              if ($response->getStatus() == 'payment_success') {

                  $this->db->update('pm_auth_item',
                      array(
                          'payment_status' => 'Void',
                          'message' => $response->getMessage(),
                          'auth_total' => 0),
                      array( 'id' => $id )
                      );
                  if (isset($_POST['id'])) {
                      // When the function executed as an independent endponit
                      $res = array();
                      $res['success'] = true;
                      return JsonResponse::create($res);
                  }
              }

            } catch (\Exception $e) {
                 print_r($e);
                 exit();
            }
    }

    /**
    * Void all the orders passed in
    */
    public function cancelAllItems()
    {
        $orders = $_POST['orders'];

        foreach ($orders as $order) {
            $this->cancelAuthItem($order['id']);
        }

        $response['success'] = true;
        return JsonResponse::create($response);
    }

    /**
    * Capture the authorized orders
    */
    public function captureOrders()
    {
      $orders = $_POST['orders'];

      foreach ($orders as $order) {
          try {
              $gateway = PaymentService::getInstance()->getGateway($order['gateway'], $this->store);
							$meta = array('orderId' => $order['id']);
              $response = $gateway->captureAuthorizedPayment($order['auth_total'], $order['transactionId'], $meta);

              if ($response->getStatus() == 'payment_success') {

                  $this->db->update('pm_auth_item',
                      array(
                          'payment_status' => 'Captured',
                          'message' => $response->getMessage(),
                          'transactionId' => $response->getTransactionId()
                          ),
                      array( 'id' => $order['id'] )
                      );
              } else {
                $this->db->update('pm_auth_item',
                    array(
                        'payment_status' => 'Failed',
                        'message' => $response->getMessage()
                        ),
                    array( 'id' => $order['id'] )
                    );
              }

          } catch (\Exception $e) {

          }
      }

      $this->db->update('pm_auth_list', array('status' => 'Completed'), array( 'id' => $orders[0]['pm_auth_list_id'] ));

      $resp['success'] = true;
      return JsonResponse::create($resp);
    }


    /**
    * get the customer information base on orderId
    */

		public function getCustomerInformation($orderId)
	 {
			 $customerStoreId = $this->customer->id;
			 $query = "
					 SELECT o.bc_id , o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email, pp.gateway_data
					 		FROM bigbackup_bc_orders o
							LEFT JOIN rc_customer_profiles cp
								 ON o.bc_customer_id = cp.store_customer_id
							LEFT JOIN rc_payment_profiles pp
								 ON cp.profile_id = pp.customer_profile_id
							WHERE o.customer_id = $customerStoreId
							AND o.bc_id = $orderId
							AND cp.customer_id = $customerStoreId
							AND pp.customer_id = $customerStoreId
							 ";

			 $info = $this->db->queryFirst($query);

			 $gateway_data = unserialize($info['gateway_data']);
			 $last4 = $gateway_data['last4'];
			 $expiry = $gateway_data['expiry'];

			 unset($info['gateway_data']);
			 $info['last4'] = $gateway_data['last4'];
			 $info['expiry'] = $gateway_data['expiry'];

			 return $info;
	 }

	 /**
	 * Export shipping fees adjustment file
	 */

	 public function adjustShippingFees($orders)
	 {
		 $header = array('Order ID', 'Customer Name', 'Customer Email', 'Shipping Fees(Allowed Max Value)','Please DO NOT change the sequence of the columns!');

		 // send file header
		 header("Content-Type: text/csv;charset=utf-8");
		 header("Content-Disposition: attachment; filename=file.csv");
		 header("Pragma: no-cache");
		 header("Expires: 0");

		 // write CSV to output stream
		 $output = fopen("php://output", "w");

		 // write header
		 fputcsv($output, $header);

		 foreach ($orders as $row) {

				 $capturedRow = array(
						 'order_id' => $row['order_id'],
						 'customer_name' => $row['customer_name'],
						 'billing_email' => $row['billing_email'],
						 // 'auth_total' => $row['auth_total'],
						 'shipping_fees' => $row['shipping_fees']
				 );
				 fputcsv($output, $capturedRow); // here you can change delimiter/enclosure

		 }

		 fclose($output);
		 exit;
	 }
    /**
    * Export authorization response list report
    */

    public function exportAuthReport($orders)
    {
        $header = array( 'Payment Status', 'Message', 'Order', 'Customer Name', 'Customer Email', 'Authorized Amount');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($orders as $row) {
            $authorizeRow = array(
                'payment_status' => $row['payment_status'],
                'message' => $row['message'],
                'order_id' => $row['order_id'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'total' => $row['auth_total']
            );
            fputcsv($output, $authorizeRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

    /**
    * Export Capture response list report
    */

    public function exportCaptureReport()
    {
        $orders = $_GET['orders'];

        $header = array('Status', 'Transaction ID', 'Failed Message', 'Order ID', 'Customer Name', 'Customer Email', 'Charge Amount');

        // send file header
        header("Content-Type: text/csv;charset=utf-8");
        header("Content-Disposition: attachment; filename=file.csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        // write CSV to output stream
        $output = fopen("php://output", "w");

        // write header
        fputcsv($output, $header);

        foreach ($orders as $row) {

            $capturedRow = array(
                'payment_status' => $row['payment_status'],
                'transactionId' => $row['transactionId'],
                'message' => $row['message'],
                'order_id' => $row['order_id'],
                'customer_name' => $row['customer_name'],
                'billing_email' => $row['billing_email'],
                'auth_total' => $row['auth_total'],
            );
            fputcsv($output, $capturedRow); // here you can change delimiter/enclosure
        }

        fclose($output);
        exit;
    }

	/**
	* Process the upload file
	*/

	public function uploadList()
	{
		$customer_id = $this->customer->id;

		try {
			$authListId = $this->processFile();
			$query = "SELECT * FROM pm_auth_item WHERE customer_id = $customer_id AND pm_auth_list_id = $authListId";

			$authItems = $this->db->query($query);
			echo json_encode($authItems);

		} catch (\Exception $e) {
			$errorMsg =  $e->getMessage();
			echo json_encode(array('Failed' => true, 'Message' => $errorMsg));
		}

	}

	/**
	*
	*/

	public function processFile()
	{
		$customer_id = $this->customer->id;

		if ($_FILES['file']['type'] != 'text/csv') {
			throw new \Exception('Only CSV file is allowed.');
		}

		$fileName = __DIR__ . $_FILES['file']['name'];

		$tempName = $_FILES['file']['tmp_name'];
		$row = 1;

		move_uploaded_file($tempName, 'tempFileLocation.csv');

		if (($handle = fopen('tempFileLocation.csv', "r")) !== FALSE) {
				while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

					if ($row == 1) {

						// We are doing the file structure validation here
						if (sizeof($data) != 5 || $data[0] != 'Order ID' || $data[3] != 'Shipping Fees(Allowed Max Value)') {
							throw new \Exception('Data is parsed in a designated format, please only upload the file which is exported from this application');
						}
					}	else {
						// When the row is bigger than 1, 7we are going to process the new shipping fees.
							$orderId = $data[0];
							$shippingFees = $data[3];
							$where = array('order_id' => $orderId, 'customer_id' => $customer_id);

							$query = "SELECT pm_auth_list_id, auth_total, shipping_fees FROM pm_auth_item WHERE order_id = $orderId AND customer_id = $customer_id";
							$order = $this->db->queryFirst($query);

							$oldShippingFees = $order['shipping_fees'];
							$authListId = $order['pm_auth_list_id'];
							$authTotal = $order['auth_total'];
							$updateAuth = $authTotal - ($oldShippingFees - $shippingFees);

							$this->db->update('pm_auth_item', array('shipping_fees' => $shippingFees, 'auth_total' => $updateAuth), $where);
					}
					$row++;
				}
				fclose($handle);
			}

			$updateOrderNum = $row - 1;

			return $authListId;

	}
}

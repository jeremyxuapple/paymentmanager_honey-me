<?php
namespace MiniBC\addons\paymentmanagerhoneyme\controllers;

use \DateTime;
use Bigcommerce\Api\Client;
use MiniBC\core\Auth;
use MiniBC\core\connection\ConnectionManager;
use MiniBC\core\entities\Addon;
use MiniBC\core\Mail;
use MiniBC\core\mail\Message;
use MiniBC\core\EntityFactory;
use MiniBC\bigcommerce\services\StorefrontAssetsService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use MiniBC\core\services\exception\WebDAVConnectionFailedException;
use MiniBC\addons\paymentmanager\services\EmailService;

class SearchController
{
    private $db = null;
    private $addon = null;
    private $categories = null;
    private $limit = null;
    private $customer = null;
    private $store = null;
    private $apiConnection = null;
    private $email_template_path;
    private $storecolor;
    private $webDavService = false;

    public function __construct()
    {
      $this->db = ConnectionManager::getInstance('mysql');
      $this->customer = Auth::getInstance()->getCustomer();
      $this->store = $this->customer->stores[0];
    }

    /**
    * Search order
    *
    * @return JsonResponse|Response
    */

    public function searchOrder()
    {
      $customer_store_id = $this->customer->id;
      $orderDate = $_GET['orderDate'];

      $search_query = "
      SELECT p.preorder_release_date, p.bc_id, o.date_created, o.bc_id AS id, o.status, o.bc_customer_id, CONCAT_WS(' ', o.billing_first_name, o.billing_last_name) AS customer_name, o.billing_email, FORMAT(o.total_inc_tax, 2) AS total_inc_tax
      FROM bigbackup_bc_orders_products op
      LEFT JOIN bigbackup_bc_products p
      ON op.product_id = p.bc_id
      LEFT JOIN bigbackup_bc_orders o
      ON o.bc_id = op.order_id
      WHERE p.customer_id = $customer_store_id
      AND op.customer_id = $customer_store_id
      AND o.customer_id = $customer_store_id
      AND p.preorder_release_date LIKE '% $orderDate %'
      AND o.status != 'Incomplete'
      GROUP BY order_id
      ORDER BY op.order_id DESC
      ";

      $orders = $this->db->query($search_query);

      // For export CSV report
      if (isset($_GET['exportCSV'])) {
          $this->exportResults($orders);
      }

      return JsonResponse::create($orders);
    }

    /**
    * Filter all the orders which are in stock and ready for immediate fulfilment
    *
    */

    public function searchInStockOrders()
    {
      $customer_store_id = $this->customer->id;
      $staff_notes = 'Status: InStock';
      $search_query = "
        SELECT bc_id AS order_id, CONCAT_WS(' ', billing_first_name, billing_last_name) AS customer_name, status, billing_email as email, total_inc_tax as total,
          SUBSTR(date_created, 6, 11) AS date
          FROM bigbackup_bc_orders
        WHERE customer_id = $customer_store_id
        AND staff_notes = '$staff_notes'";
        //
        // echo $search_query;
        // exit;

      $orders = $this->db->query($search_query);

      foreach ($orders as &$order) {
        $order['capture_amount'] = $order['total'];
      }

      return JsonResponse::create($orders);

    }




    /**
    * Export the result from the search
    *
    */

    public function exportResults($rows)
    {

      $header = array('Order', 'Order Status', 'Customer Name', 'Customer Email', 'Order Total');

      // send file header
      header("Content-Type: text/csv;charset=utf-8");
      header("Content-Disposition: attachment; filename=searchResult.csv");
      header("Pragma: no-cache");
      header("Expires: 0");

      // write CSV to output stream
      $output = fopen("php://output", "w");

      // write header
      fputcsv($output, $header);

      foreach ($rows as $row) {
           $searchRow = array(
              'order_id' => $row['id'],
              'status' => $row['status'],
              'customer_name' => $row['customer_name'],
              'billing_email' => $row['billing_email'],
              'total_inc_tax' => $row['total_inc_tax']
          );

          fputcsv($output, $searchRow); // here you can change delimiter/enclosure
      }

      fclose($output);
      exit;
    }

    /**
    * Verify if the card in on the file
    */
    public function verifyCardOnFile($customerStoreID, $customerId)
    {
        $query = "
                SELECT rpp.gateway_data
                FROM rc_customer_profiles rcp
                LEFT JOIN rc_payment_profiles rpp
                    ON rcp.profile_id = rpp.customer_profile_id
                    AND rcp.customer_id = rpp.customer_id
                WHERE rcp.customer_id = $customerStoreID
                AND rcp.store_customer_id = $customerId
                ";

        $profile = $this->db->queryFirst($query);

        return $profile['gateway_data'] != null ? true : false;
    }

    /**
    * Build proudct name list for options
    */
    public function searchOrderDate()
    {
      $customer_id = $this->customer->id;

      $orderDateQuery =
      "SELECT SUBSTRING(preorder_release_date, 6, 11) AS label, bc_id AS id
        FROM   bigbackup_bc_products
        WHERE customer_id = $customer_id
        GROUP BY SUBSTRING(preorder_release_date, 6, 11)";

      $orderDates = $this->db->query($orderDateQuery);

      unset($orderDates[0]);

      // Convert the orderDate to unixTimeFormat
      foreach ($orderDates as $i => $orderDate) {
        $orderDate['label'] = $this->unixTimeConverter($orderDate['label']);
        $orderDates[$i] = $orderDate;
      }
      // print_r($orderDates);
      // exit;
      // Sorting the date array
      $date = array();
      foreach ($orderDates as $key => $row)
      {
          $date[$key] = $row['label'];
      }
      array_multisort($date, SORT_DESC, $orderDates);

      // Convert unixTimeFormat to Readable date time
      foreach ($orderDates as $i => $orderDate) {
        $orderDate['label'] = date('d M Y', $orderDate['label']);
        $orderDates[$i] = $orderDate;
        // $orderDates[$i]['id'] = $i;
      }

      return JsonResponse::create($orderDates);
    }

    public function monthMatcher($month) {
      switch ($month) {
        case 'Jan':
          return '01';
        case 'Feb':
          return '02';
        case 'Mar':
          return '03';
        case 'Apr':
          return '04';
        case 'May':
          return '05';
        case 'Jun':
          return '06';
        case 'Jul':
          return '07';
        case 'Aug':
          return '08';
        case 'Sep':
          return '09';
        case 'Oct':
          return '10';
        case 'Nov':
          return '11';
        case 'Dec':
          return '12';
        default:
          break;
      }
    }

    // convert the time String timestamp
    public function unixTimeConverter($dateString)
    {
      $monthNum = $this->monthMatcher(substr($dateString, 3, 3));
      $dateNum = substr($dateString, 0, 2) . '-' . $monthNum  . '-' . substr($dateString, 7, 4);
      return strtotime($dateNum);
    }
}
